# EazyMove
A simple program made with Shell Script to help programmers for easy migration from computer to computer.<br />

This VERSION is for macOS only. BetaTesting Version 1.1

Note: This version used to work only on Debian-Based Linux OSs only.

To install "New Way" :- <br />
Just Download the Setup.sh file, goto the terminal and run it(./Setup.sh) !

~~Step 1: Download the file "EazyMove.sh" to any directory for now.<br />
Step 2: Open "Terminal" by pressing "Ctrl"+"Alt"+"T".<br />
Step 3: Type "cd /" to goto the root directory.<br />
Step 4: Type "chmod 777 bin" to change the permissions for the "bin" folder.<br />
Step 5: Exit "Terminal" by pressing "Alt"+"F4".<br />
Step 6: Open "File Browser".<br />
Step 7: Copy the "EazyMove.sh" file.<br />
Step 8: Paste it on /bin.<br />
Step 9: Rename the "EazyMove.sh" file to "EazyMove".<br />
Step 10: Your all set! <br />~~

To run the program:-

Step 1: Open "Terminal" by pressing "Ctrl"+"Alt"+"T".<br />
Step 2: Type "EazyMove"<br />
Step 3: Follow the instructions.<br />
