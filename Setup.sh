#!/bin/bash

# Installing BREW
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Going to Computer
cd /

# Taking Username
username=$(whoami)

# Going to home
cd /home/$username/

# Cloning GitHub Repository
git clone https://github.com/karthiksrivijay/EazyMove

# Going to EazyMove Repository
cd EazyMove

# Copying EazyMove.sh to /bin
sudo cp EazyMove.sh /bin

# Going to /bin
cd / && cd bin

# Renaming EazyMove.sh to EazyMove
sudo mv EazyMove.sh EazyMove

# Changing with chmod
sudo chmod 777 EazyMove

# Deleting Repository Files

cd /
cd /home/$username/
sudo rm -r EazyMove

# Clearing screen
clear

#  Telling user how to use.

echo "Thank you for choosing EazyMove!"
echo 'Just type "EazyMove" to run the program.'
