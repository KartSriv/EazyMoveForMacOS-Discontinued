#!/bin/bash

echo "Welcome to EazyMove!"
echo "Packages to be installed:-"
echo "1) Atom"
echo "2) Ubuntu Make"
echo "3) Visual Studio Code"
echo "4) PIP for Python 2.x and Python 3.x"
echo "5) Git for Linux"
echo "6) espeak"
echo "7) Google Chrome"
echo "8) JetBrains PyCharm"
echo "9) Deepin Desktop"

# Brew

ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null ; brew install caskroom/cask/brew-cask 2> /dev/null

# Atom

brew cask install atom # Installing Atom

# Visual Studio Code

brew cask install visual-studio-code # Installing Visual Studio Code

# PIP for Python 2.x and Python 3.x

curl -O http://python-distribute.org/distribute_setup.py

python distribute_setup.py

curl -O https://raw.github.com/pypa/pip/master/contrib/get-pip.py

python get-pip.py

# Git for linux

brew install git

# espeak

brew install espeak

# Google Chrome

brew cask install google-chrome

#PyCharm

brew cask install pycharm

# IntelliJIDEA 

brew cask install intellij-idea-ce
